# Quantum_Processors

Travaux effectués :

| Prénom Nom      | Fonction                                     |
|:---------------:|:--------------------------------------------:|
| Thomas Gréaux   | Assembleur                                   |
| Jérémy Lara     | ALU, Contrôleur (sauf catégorie B)           |
| Alexandre Redon | Banc de registre, RAM, ROM                   |
| Eric Ung        | Catégorie B du controleur, Chemin de données |
| Jihane Mesnaoui | FPGA                                         |

Modifications des circuits suite à la soutenance :  
Nous avons du modifier les circuits du contrôleur ainsi que l'organisation des branchements 
dans le processeur pour arriver à un composant fonctionnel.

+ Dans le contrôleur, nous nous sommes rendu compte qu'une partie des circuits n'était pas tout à fait fonctionnelle et que la manière dont avait été faite le contrôleur ne permettait pas une maintenabilité et un arrangement facile des circuits. C'est pourquoi le controleur a totalement été refait, en séparant les catégories dans des sous-circuits.
+ L'utilisation des multiplexers fournit dans logisim a permis une meilleure organisation et surtout a rendu le contrôleur entièrement fonctionnel et plus compréhensible.
+ Au niveau du processeur, les branchements étaient correctes, ils ont simplement été retravaillés.

Dans le dossier circuits se trouve une série d'instruction. Ce sont ces instructions là que nous aurions voulu tester lors de la soutenance. Bien sûr ces instructions auraient été d'abord écrites en langage assembleur pour être traduites en code hexadécimal à fournir à la ROM.
